import { A } from '@ember/array';
import Service from '@ember/service';
import { tracked } from "@glimmer/tracking";
import { inject as service } from '@ember/service';

export default class ServerStatusService extends Service {
  @service ninym;
  @service('event-dispatcher') events;
  @tracked data = A([]);

  constructor() {
    super(...arguments);

    // Add event listeners
    this.events.attach('DATABASE_UPDATE:server-statuses', this.update, this);
  }

  update(data) {
    this.set('data', A(data));
  }
}
