import { A } from '@ember/array';
import Service from '@ember/service';
import { tracked } from "@glimmer/tracking";
import { inject as service } from '@ember/service';

export default class InternetStatusService extends Service {
  @service ninym;
  @service('event-dispatcher') events;
  @tracked data = {
    "interface": -1,
    address: -1,
    ping: -1,
    dns: -1,
  };

  constructor() {
    super(...arguments);

    // Add event listeners
    this.events.attach('DATABASE_UPDATE:internet-status', this.update, this);
  }

  update(data) {
    this.set('data.interface', data.interface);
    this.set('data.address', data.address);
    this.set('data.ping', data.ping);
    this.set('data.dns', data.dns);
  }
}
