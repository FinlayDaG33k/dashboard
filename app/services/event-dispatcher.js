import Service from '@ember/service';

export default class EventDispatcherService extends Service {
  events = {};

  attach(name, callback, context = false) {
    // Check if we have an array for this event name
    // If not, create a new empty array
    if(!this.events[name]) this.events[name] = [];

    // Add our callback to the events list
    this.events[name].push({
      callback: callback,
      context: context
    });
  }

  detach(name, removableCallback) {
    // Make sure the event name was found
    // If not, throw an error
    if(!this.events[name]) throw new Error(`Cannot remove listener. Event "${name}" does not exist!`);

    // Remove our listener from the array
    this.events[name] = this.events[name].filter((callback) => callback !== removableCallback);
  }

  dispatch(name, data) {
    // Make sure the event name was found
    // If not, throw a warning
    if(!this.events[name]) {
      console.warn(`Dispatched event "${name}" without any listeners attached!`);
      return;
    }

    // Fire all callbacks
    this.events[name].forEach((handler) => {
      if(!handler.context) handler.callback(data);

      handler.callback.call(handler.context, data);
    });
  }
}
