import Service from '@ember/service';
import { tracked } from "@glimmer/tracking";
import { inject as service } from '@ember/service';

export default class UpsStatusService extends Service {
  @service ninym;
  @service('event-dispatcher') events;
  @tracked data;

  constructor() {
    super(...arguments);

    // Add event listeners
    this.events.attach('DATABASE_UPDATE:ups-state', this.update, this);
  }

  update(data) {
    this.set('data', data);
  }
}
