import Service from '@ember/service';
import { inject as service } from '@ember/service';
import { tracked } from "@glimmer/tracking";
import { storageFor } from "ember-local-storage";

export default class NinymService extends Service {
  @storageFor('settings') settings;
  @service('websockets') websockets;
  @service notifications;
  @service('crypto-ticker') cryptoTicker;
  @service('server-status') serverStatus;
  @service('event-dispatcher') events;
  @service('schedule') schedule;
  @tracked socketState = 0;
  socketRef = null;

  constructor() {
    super(...arguments);

    // Connect to the socket server
    this.connect();
  }

  connect() {
    // Connect to the socket server
    const socket = this.websockets.socketFor(`${this.settings.get('ninymWebsocket')}/${this.settings.get('ninymToken')}`);

    socket.on('close', () => {
      this.set('socketState', -1);
    }, this);

    socket.on('error', () => {
      this.set('socketState', -1);
    }, this)

    socket.on('open', () => {
      console.log('Connected to Ninym Websocket!');
      this.set('socketState', 1);
    }, this);

    socket.on('message', (e) => {
      // Parse our JSON message
      let data = JSON.parse(e.data);

      // Check if it's a known event
      // If so, dispatch the appropriate event
      switch(data.event) {
        case 'DATABASE_UPDATE':
          let content = data.data.content;
          try {
            content = JSON.parse(data.data.content);
          } catch(e) {

          }
          if(!content) break;
          this.events.dispatch(`DATABASE_UPDATE:${data.data.key}`, content);
          break;
        default:
          this.events.dispatch(data.event, data.data);
      }

    }, this);

    this.set('socketRef', socket);
  }

  reconnect() {
    this.set('socketState', 0);
    this.connect();
  }

  sendMessage(event, data) {
    this.socketRef.send({
      event: event,
      data: data
    }, true);
  }
}
