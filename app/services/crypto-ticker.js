import { A } from '@ember/array';
import Service from '@ember/service';
import { tracked } from "@glimmer/tracking";
import { inject as service } from '@ember/service';

export default class CryptoTickerService extends Service {
  @service ninym;
  @service('event-dispatcher') events;
  @tracked data = A([]);

  constructor() {
    super(...arguments);

    // Add event listeners
    this.events.attach('DATABASE_UPDATE:crypto-tickers', this.update, this);
  }

  update(ticker) {
    this.set('data', A(ticker));
  }
}
