import { A } from '@ember/array';
import Service from '@ember/service';
import {tracked} from "@glimmer/tracking";
import { inject as service } from '@ember/service';
import { storageFor } from 'ember-local-storage';

export default class NotificationsService extends Service {
  @storageFor('settings') settings;
  @service ninym;
  @service('event-dispatcher') events;
  @tracked items = A([]);
  @tracked errorLevel = -1;

  constructor() {
    super(...arguments);

    // Add event listeners
    this.events.attach('NOTIFICATION_UPDATE', this.update, this);
  }

  clear() {
    this.set('items', A([]));
  }

  update(notifications) {
    this.set('items', A(notifications));

    // Update the error level
    this.getErrorLevel();
  }

  dismiss(id) {
    console.log(`Dismissing notification #${id}`);
    this.ninym.sendMessage('DISMISS_NOTIFICATION', {
      id: id,
      auth: this.settings.get('ninymToken')
    })
  }

  dismissAll() {
    console.log(`Dismissing all notifications`);
    this.ninym.sendMessage('DISMISS_ALL_NOTIFICATION');
  }

  getErrorLevel() {
    // Check if we have errors
    if(this.items.filter(notification => notification.severity === 'error').length > 0) {
      this.set('errorLevel', 2);
      return;
    }

    // Check if we have warnings
    if(this.items.filter(notification => notification.severity === 'warning').length > 0) {
      this.set('errorLevel', 1);
      return;
    }

    // Check if we have info
    if(this.items.filter(notification => notification.severity === 'info').length > 0) {
      this.set('errorLevel', 0);
      return;
    }

    // Nothing appears to be here
    this.set('errorLevel', -1);
  }
}
