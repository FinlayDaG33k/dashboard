import Service from '@ember/service';
import { tracked } from "@glimmer/tracking";
import { inject as service } from '@ember/service';

export default class WeatherService extends Service {
  @service ninym;
  @service('event-dispatcher') events;
  @tracked current = {
    temperature: 0,
    weather: {
      name: 'Sunny',
      id: 0
    },
    precipitation: 0,
    humidity: 0,
    wind: {
      speed: 0,
      direction: 'ne'
    },
    sunrise: 0,
    sunset: 0,
    uvi: 0
  };

  constructor() {
    super(...arguments);

    // Add event listeners
    this.events.attach('DATABASE_UPDATE:weather', this.update, this);
  }

  update(weather) {
    this.set('current.temperature', weather.current.temp);
    this.set('current.humidity', weather.current.humidity);
    this.set('current.wind.speed', weather.current.wind_speed);
    this.set('current.wind.direction', this.windHeading(weather.current.wind_deg));
    this.set('current.sunrise', this.sunTime(weather.current.sunrise));
    this.set('current.sunset', this.sunTime(weather.current.sunset));
    this.set('current.uvi', weather.current.uvi);
    this.set('current.weather.id', weather.current.weather[0].id);
    this.set('current.weather.name', weather.current.weather[0].main);
  }

  /**
   * Source: https://stackoverflow.com/a/25867068
   *
   * @param deg
   * @return {string}
   */
  windHeading(deg) {
    const val = Math.floor((deg / 22.5) + 0.5);
    const headings = ["n", "nne", "ne", "ene", "e", "ese", "se", "sse", "s", "ssw", "sw", "wsw", "w", "wnw", "nw", "nnw"];
    return headings[(val % headings.length)];
  }

  sunTime(timestamp) {
    let date = new Date(timestamp * 1000),
      min = date.getMinutes(),
      sec = date.getSeconds(),
      hour = date.getHours();

    return  (hour < 10 ? ("0" + hour) : hour) + ":" +
      (min < 10 ? ("0" + min) : min) + ":" +
      (sec < 10 ? ("0" + sec) : sec);
  }
}
