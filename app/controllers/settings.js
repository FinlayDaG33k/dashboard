import Controller from '@ember/controller';
import { action } from '@ember/object';
import { storageFor } from 'ember-local-storage';

export default class SettingsController extends Controller {
  @storageFor('settings') settings;

  @action
  updateNinymToken(e) {
    // Make sure we only catch enter keys
    if (e.keyCode !== 13) return;

    // Update the token in the localStorage
    this.settings.ninymToken = e.target.value;
  }

  @action
  updateNinymWebsocket(e) {
    // Make sure we only catch enter keys
    if (e.keyCode !== 13) return;

    // Update the token in the localStorage
    this.settings.ninymWebsocket = e.target.value;
  }
}
