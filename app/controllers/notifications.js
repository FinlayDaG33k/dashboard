import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';

export default class NotificationsController extends Controller {
  @service('notifications') notifications;

  @action
  dismissAll() {
    this.notifications.dismissAll();
  }
}
