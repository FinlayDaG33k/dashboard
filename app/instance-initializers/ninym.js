const services = [
  'event-dispatcher',
  'crypto-ticker',
  'fuel-prices',
  'notifications',
  'rss-feed',
  'schedule',
  'server-status',
  'ups-status',
  'weather',
  'internet-status',
];

export function initialize(app) {
  const { container = app} = app;

  // Register all service listeners
  for(const service of services) {
    container.lookup(`service:${service}`);
  }


  // Initialize Ninym Websocket
  container.lookup('service:ninym');
}

export default { initialize };
