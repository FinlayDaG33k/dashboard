import EmberRouter from '@ember/routing/router';
import config from 'dashboard/config/environment';

export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}

Router.map(function() {
  this.route('home', {path:'/'});
  this.route('notifications');
  this.route('finance');
  this.route('settings');
});
