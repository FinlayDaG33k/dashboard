import Route from '@ember/routing/route';
import { schedule } from '@ember/runloop';

export default class HomeRoute extends Route {
  model() {
    return {
      daily: [
        {name: 'Facebook', url: 'https://www.facebook.com'},
        {name: 'YouTube', url: 'https://www.youtube.com'},
        {name: 'Mastodon', url: 'https://social.linux.pizza'},
        {name: 'devRant', url: 'https://devrant.com'},
        {name: 'Instagram', url: 'https://www.instagram.com'}
      ],
      development: [
        {name: 'GitLab', url: 'https://www.gitlab.com'},
        {name: 'Localhost (HTTPS)', image: 'nginx', url: 'https://localhost'},
        {name: 'Localhost (Ember)', image: 'ember', url: 'http://localhost:4200'},
        {name: 'Localhost PhpMyAdmin', image: 'phpmyadmin', url: 'http://localhost:8082'}
      ],
      projects: [
        {name: 'Website', image: 'gitlab', url: 'https://www.gitlab.com/finlaydag33k/website'},
        {name: 'Edinburgh', image: 'gitlab', url: 'https://gitlab.com/the-mermaids-rock/development/edinburgh'},
        {name: 'deno-lib', image: 'github', url: 'https://github.com/FinlayDaG33k/deno-lib'},
        {name: 'Dashboard', image: 'gitlab', url: 'https://www.gitlab.com/finlaydag33k/dashboard'},
        {name: 'Ninym', image: 'gitlab', url: 'https://gitlab.com/FinlayDaG33k/ninym/'},
        {name: 'TMR (Group)', image: 'gitlab', url: 'https://gitlab.com/the-mermaids-rock'},
      ],
      reddit: [
        {name: 'r/FinlayDaG33k', image: 'reddit', url: 'https://www.reddit.com/r/finlaydag33k'},
      ]
    }
  }

  activate() {
    schedule('afterRender', () => {
      const searchBar = document.querySelector('#search-input');
      document.addEventListener('keydown', (event) => {
        // Check if we're already focussing the searchbar
        if(document.activeElement == searchBar) return;

        // Focus the searchbar
        searchBar.focus();
      });
    });
  }
}
