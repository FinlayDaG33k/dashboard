import Component from '@glimmer/component';
import { inject as service } from '@ember/service';

export default class CryptoTickerComponent extends Component {
  @service('crypto-ticker') ticker;

  constructor() {
    super(...arguments);
  }
}
