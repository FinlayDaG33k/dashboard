import Component from '@glimmer/component';
import { inject as service } from '@ember/service';

export default class FuelPricesComponent extends Component {
  @service('fuel-prices') prices;

  constructor() {
    super(...arguments);
  }
}
