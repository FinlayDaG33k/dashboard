import Component from '@glimmer/component';
import { inject as service } from '@ember/service';

export default class InternetStatusComponent extends Component {
  @service('internet-status') statuses;

  constructor() {
    super(...arguments);
  }
}
