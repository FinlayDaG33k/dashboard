import Component from '@glimmer/component';
import { inject as service } from '@ember/service';

export default class ServerStatusComponent extends Component {
  @service('server-status') statuses;

  constructor() {
    super(...arguments);
  }
}
