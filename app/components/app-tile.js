import Component from "@glimmer/component";

export default class AppTileComponent extends Component {
  name = '';
  url = '';
  image = '';

  constructor(...args) {
    super(...args);

    this.name = this.args.name;
    this.url = this.args.url;
    this.image = this.args.image;
  }

  get icon() {
    if(!this.image) return this.name.toLowerCase();
    return this.image;
  }
}
