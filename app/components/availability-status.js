import Component from "@glimmer/component";
import { tracked } from "@glimmer/tracking";
import { inject as service } from '@ember/service';
import { action } from '@ember/object';

export default class AvailabilityStatusComponent extends Component {
  @service ninym;
  @service('event-dispatcher') events;

  @tracked current = 'unknown';
  options = [
    {status: 'available', label: 'Desk', icon: 'desktop-classic'},
    {status: 'busy', label: 'Busy', icon: 'alert-circle-outline'},
    {status: 'chilling', label: 'Chilling', icon: 'cast-variant'},
    {status: 'brb', label: 'Be Right Back', icon: 'alarm'},
    {status: 'eating', label: 'Eating', icon: 'food-variant'},
    {status: 'nap', label: 'Nap', icon: 'sleep'},
    {status: 'live', label: 'Live', icon: 'video'},
    {status: 'afk', label: 'Away', icon: 'logout-variant'},
  ];

  constructor(...args) {
    super(...args);

    // Add event listeners
    this.events.attach('DATABASE_UPDATE:availability-status', this.event, this);
  }

  @action
  update(status) {
    this.ninym.sendMessage('UPDATE_AVAILABILITY', {status: status});
  }

  event(status) {
    this.current = status;
  }
}
