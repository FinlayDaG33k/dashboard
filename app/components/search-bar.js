import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class SearchbarComponent extends Component {
  @tracked indicator = 'g';

  selectors = {
    g: `https://www.google.com/search?q={{QUERY}}`,
    img: `https://lens.google.com/uploadbyurl?url={{QUERY}}`,
    site: `https://{{QUERY}}`,
    tpu: `https://www.google.com/search?q=site%3Atechpowerup.com+{{QUERY}}+Specs`,
    yt: `https://www.youtube.com/results?search_query={{QUERY}}`,
  };

  constructor(...args) {
    super(...args);
  }

  /**
   * Handle on keydown events in the input.
   * Catch the focus if we didn't focus the searchbar yet.
   * keyCodes we are interested in:
   * - 9: Tab
   * - 13: Enter
   * - 229: ???
   *
   * @param e
   * @return void
   */
  @action
  onKeyDown(e) {
    // Make sure we are not composing or whatever 229 does
    if (e.isComposing || e.keyCode === 229) return;

    // Handle the codes are are interested in
    switch (e.keyCode) {
      case 9: {
        // Prevent tabbing out of the input
        if (document.activeElement !== e.target) return;
        e.preventDefault();

        // Check if we want to change selector
        let selector = (e.target.value).toLowerCase();
        if (!Object.prototype.hasOwnProperty.call(this.selectors, selector)) return;

        // Change the indicator
        this.indicator = selector;

        // Clear the current content
        e.target.value = "";
        break;
      }
      case 13: {
        // Get our query
        const query = e.target.value;

        // Make sure something was entered
        if(query === "") return;

        // Get our target url and navigate to it
        window.location.href = this.target(query);
        break;
      }
      default: {
        // Check if we're already focussing the searchbar
        // If so, return now
        if(document.activeElement === e.target) return;

        // Focus the searchbar
        e.target.focus();
        break;
      }
    }
  }

  /**
   * Get the target from our indicator status
   * Then parse the URL and add in our query
   *
   * @param query
   * @returns {string}
   */
  target(query) {
    let target;
    switch(this.indicator) {
      case 'g': {
        target = this.selectors.g.replace('{{QUERY}}', encodeURIComponent(query));
        break;
      }
      case 'img': {
        target = this.selectors.img.replace('{{QUERY}}', encodeURIComponent(query));
        break;
      }
      case 'site': {
        target = this.selectors.site.replace('{{QUERY}}', query);
        break;
      }
      case 'tpu': {
        target = this.selectors.tpu.replace('{{QUERY}}', encodeURIComponent(query));
        break;
      }
      case 'yt': {
        target = this.selectors.yt.replace('{{QUERY}}', encodeURIComponent(query));
        break;
      }
      default: {
        target = this.selectors.g.replace('{{QUERY}}', encodeURIComponent(query));
        break;
      }
    }

    return target;
  }
}
