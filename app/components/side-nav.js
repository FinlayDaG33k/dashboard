import Component from "@glimmer/component";
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { htmlSafe } from '@ember/template';

export default class SideNavComponent extends Component {
  @service notifications;
  @service ninym;

  // Define sidebar services
  services = [
    {name: 'Plex', url: 'https://app.plex.tv/desktop'},
    {name: 'Portainer', url: 'http://192.168.3.122:9000/'},
    {name: 'Proxmox', url: 'https://isoroku.finlaydag33k.local:8006/'},
    {name: 'FreeNAS', url: 'https://nas.finlaydag33k.local/'},
    {name: 'Twitch', url: 'https://dashboard.twitch.tv/u/finlaydag33k/stream-manager'},
    {name: 'Resilio', url: 'http://rsl.finlaydag33k.local:8888/gui/'}
  ];

  constructor(...args) {
    super(...args);
  }

  @action
  ninymConnect() {
    if(this.ninym.socketState !== -1) return;
    this.ninym.reconnect();
  }

  get ninymIconColor() {
    switch(this.ninym.socketState) {
      case -1:
        return htmlSafe("color: var(--danger)");
      case 0:
        return htmlSafe("color: var(--warning)");
      case 1:
        return htmlSafe("color: var(--success)");
      default:
        return htmlSafe("color: var(--warning)");
    }
  }

  get ninymDisconnected() {
    return this.ninym.socketState === 0;
  }

  get notificationColor() {
    switch(this.notifications.errorLevel) {
      case -1:
        return htmlSafe("color: var(--white)");
      case 0:
        return htmlSafe("color: var(--blue)");
      case 1:
        return htmlSafe("color: var(--warning)");
      case 2:
        return htmlSafe("color: var(--danger)");
      default:
        return htmlSafe("color: var(--white)");
    }
  }
}
