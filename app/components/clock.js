import Component from "@glimmer/component";
import { tracked } from "@glimmer/tracking";
import { later } from "@ember/runloop";

export default class ClockComponent extends Component {
  @tracked display = '';

  constructor(...args) {
    super(...args);
    this.display = this.getTime();
    this.updateTime();
  }

  updateTime() {
    later(this, () => {
      this.display = this.getTime();
      this.updateTime();
    }, 1000);
  }

  getTime() {
    let date = new Date(),
      year = date.getFullYear(),
      month = date.getMonth(),
      day = date.getDate(),
      min = date.getMinutes(),
      sec = date.getSeconds(),
      hour = date.getHours();

    return ('0' + day).slice(-2) + '/' +
      ('0' + month).slice(-2) + '/' +
      year +
      ' ' +
      (hour < 10 ? ("0" + hour) : hour) + ":" +
      (min < 10 ? ("0" + min) : min) + ":" +
      (sec < 10 ? ("0" + sec) : sec);
  }
}
