import Component from "@glimmer/component";

export default class SidebarIconComponent extends Component {
  url = '';

  constructor(...args) {
    super(...args);

    this.url = this.args.url;
  }

  get icon() {
    return this.args.name.toLowerCase();
  }
}
