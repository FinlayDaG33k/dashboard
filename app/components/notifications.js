import Component from "@glimmer/component";
import { inject as service } from '@ember/service';
import { action } from '@ember/object';

export default class NotificationsComponent extends Component {
  @service('notifications') notifications;

  constructor(...args) {
    super(...args);
  }

  @action
  dismiss(id) {
    this.notifications.dismiss(id);
  }
}
