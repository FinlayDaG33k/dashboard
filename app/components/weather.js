import Component from "@glimmer/component";
import { inject as service } from '@ember/service';

export default class Weather extends Component {
  @service('weather') weather;

  constructor(...args) {
    super(...args);
  }
}
