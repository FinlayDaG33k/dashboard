import Component from '@glimmer/component';
import { inject as service } from '@ember/service';

export default class RssFeedComponent extends Component {
  @service('rss-feed') feed;

  constructor() {
    super(...arguments);
  }
}
