import Component from '@glimmer/component';

export default class QuickAccessComponent extends Component {
  items = [];

  constructor() {
    super(...arguments);
    this.items = this.args.items;
  }
}
