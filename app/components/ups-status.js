import Component from '@glimmer/component';
import { inject as service } from '@ember/service';

export default class UpsStatusComponent extends Component {
  @service('ups-status') status;

  constructor() {
    super(...arguments);
  }
}
