import { helper } from '@ember/component/helper';
import { htmlSafe } from '@ember/template';

function serverstatus([value]) {
  switch(value.toLowerCase()) {
    case 'up':
      return htmlSafe(`color: var(--success)`);
    case 'down':
      return htmlSafe(`color: var(--danger)`);
    default:
      return htmlSafe(`color: var(--warning)`);
  }
}

export default helper(serverstatus);
