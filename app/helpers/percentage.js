import { helper } from '@ember/component/helper';

function percentage([value, decimals = 2]) {
  return (value * 100).toFixed(decimals);
}

export default helper(percentage);
