import { helper } from '@ember/component/helper';
import { htmlSafe } from '@ember/template';

function cryptostatus([value]) {
  if(value > 0) return htmlSafe(`<i class="fas fa-arrow-up" style="color: var(--success)"></i>`);
  if(value === 0) return htmlSafe(`<i class="fas fa-minus" style="color: var(--warning)"></i>`);
  if(value < 0) return htmlSafe(`<i class="fas fa-arrow-down" style="color: var(--danger)"></i>`);
}

export default helper(cryptostatus);
