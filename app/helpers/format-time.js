import { helper } from '@ember/component/helper';

function formatTime([value]) {
  let date = new Date(value),
    year = date.getFullYear(),
    month = date.getMonth(),
    day = date.getDate(),
    min = date.getMinutes(),
    sec = date.getSeconds(),
    hour = date.getHours();

  return ('0' + day).slice(-2) + '/' +
    ('0' + (month  + 1)).slice(-2) + '/' +
    year +
    ' ' +
    (hour < 10 ? ("0" + hour) : hour) + ":" +
    (min < 10 ? ("0" + min) : min) + ":" +
    (sec < 10 ? ("0" + sec) : sec);
}

export default helper(formatTime);
