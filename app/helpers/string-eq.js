import { helper } from '@ember/component/helper';

function stringEq([a, b]) {
  return a === b;
}

export default helper(stringEq);
