import { helper } from '@ember/component/helper';
import { htmlSafe } from '@ember/template';

function internetstatus([value]) {
  console.log(value)
  switch(value) {
    case -1:
      return htmlSafe(`color: var(--gray-dark)`);
    case 0:
      return htmlSafe(`color: var(--danger)`);
    case 1:
      return htmlSafe(`color: var(--success)`);
    case 2:
      return htmlSafe(`color: var(--indigo)`);
    default:
      return htmlSafe(`color: var(--warning)`);
  }
}

export default helper(internetstatus);
