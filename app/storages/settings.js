import StorageObject from 'ember-local-storage/local/object';

const Storage = StorageObject.extend();

Storage.reopenClass({
  initialState() {
    return {
      ninymWebsocket: 'wss://ninym.finlaydag33k.nl'
    };
  }
});

export default Storage;
