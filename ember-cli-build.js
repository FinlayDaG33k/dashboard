'use strict';

const EmberApp = require('ember-cli/lib/broccoli/ember-app');

module.exports = function(defaults) {
  let app = new EmberApp(defaults, {
    'ember-web-app': {
      enabled: true
    },
    outputPaths: {
      app: {
        css: {
          'app': '/assets/dashboard.css',
          'themes/default': '/assets/themes/default.css',
          'vendor/bootstrap/bootstrap': '/assets/vendor/bootstrap.css',
          'vendor/fontawesome/fontawesome': '/assets/vendor/fontawesome.css',
          'vendor/weather-icons/weather-icons': '/assets/vendor/weather-icons.css',
          'vendor/weather-icons/weather-icons-wind': '/assets/vendor/weather-icons-wind.css',
        }
      }
    },
    brotli: {
      extensions: ['js', 'css', 'html'],
      keepUncompressed: true,
      appendSuffix: true
    },
    'asset-cache': {
      include: [
        'assets/**/*',
        'font/**/*',
        'img/**/*.webp'
      ]
    }
  });

  // Use `app.import` to add additional libraries to the generated
  // output files.
  //
  // If you need to use different assets in different
  // environments, specify an object as the first parameter. That
  // object's keys should be the environment name and the values
  // should be the asset to use in that environment.
  //
  // If the library that you are including contains AMD or ES6
  // modules that you would like to import into your application
  // please specify an object with the list of modules as keys
  // along with the exports of each module as its value.

  return app.toTree();
};
