'use strict';

module.exports = function(/* environment, appConfig */) {
  return {
    name: "Ninym Dashboard",
    short_name: "Ninym",
    description: "Dashboard for accessing and controlling the main Ninym instance",
    start_url: "/",
    scope: "/",
    display: "standalone",
    background_color: "#000f1f",
    theme_color: "#fff",
    icons: [
    ],
    ms: {
      tileColor: '#fff'
    }
  };
}
